import os
from utility.Env import Env

class Base:

    def data(self, db):
        self.db_name = db
        self.db = Base.create(os.path.join(Env().PATH + 'data', db))
        self.set = Base.create(os.path.join(self.db, 'set'))
        return self

    def result(self, db, set, model):
        self.db_name = db
        self.db = Base.create(os.path.join(Env().PATH + 'result', db))
        self.set = Base.create(os.path.join(self.db, set))
        self.model = Base.create(os.path.join(self.set, model))
        return self

    @staticmethod
    def create(path):
        if not os.path.exists(path):
            os.mkdir(path)
        return path
