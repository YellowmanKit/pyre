class Rating:
    def __init__(self, rating):
        self.value = rating
        self.u_idx = rating['u_idx']
        self.e_idx = rating['e_idx']
        self.rating = rating['rating']
        self.is_helper = rating['is_helper']
