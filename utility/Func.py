from utility.Class import Rating
import numpy as np
import pandas as pd
import random
import torch as tt

class Func:

    def load_kg_triples(triples_path):
        with open(triples_path) as fp:
            df = pd.read_csv(fp)
            triples = [(h, r, t) for h, r, t in df[['head', 'relation', 'tail']].values]

        r_idx_map = {}
        rc = 0
        for h, r, t in triples:
            if r not in r_idx_map:
                r_idx_map[r] = rc
                rc += 1

        return triples, len(r_idx_map)

    @staticmethod
    def flatten_ratings(user_ratings, helpers_only=False):
        rating_triples = []
        for u, rs in user_ratings:
            rating_triples += (
                [(r.u_idx, 1 if r.rating == 1 else 0, r.e_idx) for r in rs if r.is_helper] if helpers_only else
                [(r.u_idx, 1 if r.rating == 1 else 0, r.e_idx) for r in rs]
            )
        return rating_triples

    @staticmethod
    def get_like_matrix(ratings, n_users, n_entities):
        u_idx_to_matrix_map, uc = {}, 0
        e_idx_to_matrix_map, ec = {}, 0

        R = np.zeros((n_users, n_entities))
        for u, r, e in ratings:
            if u not in u_idx_to_matrix_map:
                u_idx_to_matrix_map[u] = uc
                uc += 1
            if e not in e_idx_to_matrix_map:
                e_idx_to_matrix_map[e] = ec
                ec += 1

            R[u_idx_to_matrix_map[u]][e_idx_to_matrix_map[e]] = 1 if r == 1 else -1

        return R, u_idx_to_matrix_map, e_idx_to_matrix_map

    @staticmethod
    def evaluate_hit(model, user_samples, n=10):
        with tt.no_grad():
            model.eval()
            # Each entry is (user, (pos_sample, neg_samples))
            ranks = []
            dcgs = []
            for user, (pos_sample, neg_samples) in user_samples:
                fast_rank = model.fast_rank(user, 1, pos_sample, neg_samples)
                fast_dcg = Func.dcg(fast_rank, n=n)
                ranks.append(fast_rank)
                dcgs.append(fast_dcg)

            _dcg = np.mean(dcgs)
            _hit = len(np.where(np.array(ranks) < n)[0]) / len(user_samples)

            return float(_hit), float(_dcg)

    @staticmethod
    def corrupt_std(flat_ratings, all_entities):
        corrupted = []
        for h, r, t in flat_ratings:
            if random.random() > 0.5:
                corrupted.append((random.choice(all_entities), r, t))
            else:
                corrupted.append((h, r, random.choice(all_entities)))

        return corrupted

    @staticmethod
    def corrupt_rating_triples(triples, ratings_matrix, u_idx_to_matrix_map, e_idx_to_matrix_map):
        corrupted = []
        for h, r, t in triples:
            h_mat = u_idx_to_matrix_map[h]
            t_mat = e_idx_to_matrix_map[t]
            if random.random() > 0.5:
                if r == 1:
                    # Find a user that dislikes t
                    users_disliking_t = np.argwhere(ratings_matrix[:, t_mat] == -1).flatten()
                    if len(users_disliking_t) == 0:
                        users_disliking_t = list(e_idx_to_matrix_map.keys())
                    corrupted.append((random.choice(users_disliking_t), r, t))
                else:
                    # Find a user that likes t
                    users_liking_t = np.argwhere(ratings_matrix[:, t_mat] == 1).flatten()
                    if len(users_liking_t) == 0:
                        users_liking_t = list(e_idx_to_matrix_map.keys())
                    corrupted.append((random.choice(users_liking_t), r, t))
            else:
                if r == 1:
                    # Find an item that h dislikes
                    items_disliked_by_h = np.argwhere(ratings_matrix[h_mat] == -1).flatten()
                    if len(items_disliked_by_h) == 0:
                        items_disliked_by_h = list(e_idx_to_matrix_map.keys())
                    corrupted.append((h, r, random.choice(items_disliked_by_h)))
                else:
                    # Find an item that h likes
                    items_liked_by_h = np.argwhere(ratings_matrix[h_mat] == 1).flatten()
                    if len(items_liked_by_h) == 0:
                        items_liked_by_h = list(e_idx_to_matrix_map.keys())
                    corrupted.append((h, r, random.choice(items_liked_by_h)))

        return corrupted

    @staticmethod
    def batchify(pos, neg, batch_size=64):
        for i in range(0, len(pos), batch_size):
            yield zip(*pos[i:i + batch_size]), zip(*neg[i:i + batch_size])

    @staticmethod
    def dcg(rank, n=10):
        r = np.zeros(n)
        if rank < n:
            r[rank] = 1

        return r[0] + np.sum(r[1:] / np.log2(np.arange(2, r.size + 1)))
