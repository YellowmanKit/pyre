import requests
import json
from utility.Env import Env

class Query:

    @staticmethod
    def post(q, body=None):
        res = requests.post(Env().api + q, json=body)
        data = json.loads(res.text)
        return data['result']
