class Env:
    def __init__(self):
        self.value = {}
        self.PATH = r'C:\Users\HINCare\Desktop\Projects\pyre\\'
        data = open(self.PATH + '.env').read().split('\n')
        for value in data:
            splited = value.split('=')
            if len(splited) == 2:
                self.value[splited[0]] = splited[1]
        self.api = self.value['API']
