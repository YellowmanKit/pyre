from model.TransE import TransE
from utility.Func import Func
import torch as tt
import pickle
import random
import numpy as np

class KGTransE:
    def __init__(self, split):
        self.split = split
        self.number = {
            'entities': split.n_users + split.n_helpers + split.n_descriptive_entities,
            'relations': 9
        }
        self.option = {
            'margin': 1,
            'learning_rate': 0.03,
            'with_kg_triples': True,
            'with_standard_corruption': True,
            'max_iterations': 10
        }
        self.best = {
            'hit': 0,
            'k': None,
            'model': None
        }
        for n_latent_factors in [50, 100, 250]:
        #for n_latent_factors in [50]:
            self.fit(n_latent_factors)
        self.fit(self.best['k'])

    def fit(self, k):
        model = TransE(self.number, self.option, k)
        val_hit_history = []
        val_dcg_history = []

        train = self.split.training

        kg_triples, n_relations = Func.load_kg_triples(self.split.set.data.triples_path) if self.option['with_kg_triples'] else ([], {})
        #self.number['relations'] += n_relations

        optimizer = tt.optim.Adam(model.parameters(), lr=self.option['learning_rate'])

        all_train_ratings = Func.flatten_ratings(train)
        ratings_matrix, u_idx_to_matrix_map, e_idx_to_matrix_map = Func.get_like_matrix(
            all_train_ratings,
            self.split.n_users,
            self.split.n_entities)

        for epoch in range(self.option['max_iterations']):
            if epoch % 1 == 0:
                _hit, _dcg = Func.evaluate_hit(model, self.split.validation, n=10)
                if _hit > self.best['hit']:
                    self.best['hit'] = _hit
                    self.best['k'] = model.k
                    self.best['model'] = pickle.loads(pickle.dumps(model))
                val_hit_history.append(_hit)
                val_dcg_history.append(_dcg)

            corrupted_train_ratings = (
                Func.corrupt_std(all_train_ratings, range(self.number['entities']))
                if self.option['with_standard_corruption'] else
                Func.corrupt_rating_triples(all_train_ratings, ratings_matrix, u_idx_to_matrix_map, e_idx_to_matrix_map))
            corrupted_train_kg_triples = Func.corrupt_std(kg_triples, range(self.number['entities']))

            all_pairs = list(zip(all_train_ratings, corrupted_train_ratings))
            all_pairs += list(zip(kg_triples, corrupted_train_kg_triples))[:len(all_pairs)]

            random.shuffle(all_pairs)
            positive_samples, negative_samples = zip(*all_pairs)

            model.train()
            for (p_h, p_r, p_t), (n_h, n_r, n_t) in Func.batchify(positive_samples, negative_samples):
                p_distance = model(p_h, p_r, p_t)
                n_distance = model(n_h, n_r, n_t)

                loss = tt.relu(model.margin + p_distance - n_distance).sum()
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()

        return np.mean(val_hit_history[-10:])

    def predict(self, user, items):
        with tt.no_grad():
            self.best['model'].eval()
            return self.best['model'].predict_helpers_for_user(user, relation_idx=1, movie_indices=items)
