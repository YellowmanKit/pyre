import random

class Operation:

    def sample(self, array):
        sample = random.choice(array)
        array.remove(sample)
        return sample

    def sample_negative(self, all, seen, n):
        unseen = list(all - seen)
        random.shuffle(unseen)
        return unseen[:n]

    def remove(self, item, array):
        if item in array:
            array.remove(item)
