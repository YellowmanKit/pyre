import json
from os.path import join
from utility.Base import Base
from model.recommander.KGTransE import KGTransE
from utility.Query import Query

class Suggest:

    def gen(self, data):
        base = Base().result(data.db_name, 'suggest', 'transe-kg')
        for set in data.sets():
            for split in set.splits():
                if len(split.training) == 0 or len(split.testing) == 0 or len(split.validation) == 0:
                    print('skip split')
                    continue
                suggestion = self.run(base, KGTransE(split))

    def run(self, base, model):
        result = {}
        users = []
        for u, rs in model.split.training:
            users += [u]
        for u in users:
            _users = users.copy()
            _users.remove(u)
            predictions = model.predict(u, _users).items()
            predictions = sorted(predictions, key=lambda item: item[1], reverse=True)
            result[u] = {}
            for u2, s in predictions:
                result[u][u2.item()] = s.item()
        with open(join(base.model, 'suggest.json'), 'w') as fp:
            json.dump(result, fp)
        self.feedback(result, base)

    def feedback(self, result, base):
        print('feedback')
        mapping = self.mapping(base.db_name)
        mapped_result = {}
        for elder in result:
            elder_id = mapping[str(elder)]
            mapped_result[elder_id] = {}
            for helper in result[elder]:
                helper_id = mapping[str(helper)]
                mapped_result[elder_id][helper_id] = result[elder][helper]
        for user in mapped_result:
            res = Query.post('feedback/' + base.db_name, { user: mapped_result[user] } )

    def mapping(self, db):
        data_base = Base().data(db)
        with open(join(data_base.db, 'mapping.json')) as json_file:
            mapping = json.load(json_file)
            return mapping
