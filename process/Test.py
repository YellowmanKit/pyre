import json
import numpy as np
from os.path import join
from collections import defaultdict
from utility.metrics import ndcg_at_k
from utility.Base import Base
from process.Summary import summarise
from model.recommander.KGTransE import KGTransE

class Test:

    def run(self, data):
        for set in data.sets():
            base = Base().result(data.db_name, set.name, 'transe-kg')
            for split in set.splits():
                hr, ndcg = self.recommander(KGTransE(split))
                with open(join(base.model, split.name), 'w') as fp:
                    json.dump({'hr': hr, 'ndcg': ndcg}, fp)
                    print('dump ' + split.name)
            summarise(base.set)

    def recommander(self, model, upper_cutoff=50):
        hits = defaultdict(list)
        ndcgs = defaultdict(list)

        for u, (pos_sample, neg_samples) in model.split.testing:
            predictions = model.predict(u, neg_samples + [pos_sample]).items()
            predictions = sorted(predictions, key=lambda item: item[1], reverse=False)
            #print(predictions)
            relevance = [1 if item == pos_sample else 0 for item, score in predictions]

            # Append hits and NDCG for various cutoffs
            for k in range(1, upper_cutoff + 1):
                cutoff = relevance[:k]

                hits[k].append(1 in cutoff)
                ndcgs[k].append(ndcg_at_k(cutoff, k))

        # Transform lists to means
        hr = dict()
        ndcg = dict()

        for k in range(1, upper_cutoff + 1):
            hr[k] = np.mean(hits[k])
            ndcg[k] = np.mean(ndcgs[k])

        return hr, ndcg
