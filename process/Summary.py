import os
import json
import numpy as np
from collections import defaultdict

def get_summary(experiment_base, upper_cutoff=50):
    model_directories = []
    for directory in os.listdir(experiment_base):
        if not os.path.isdir(os.path.join(experiment_base, directory)):
            continue

        model_directories.append(directory)

    if not model_directories:
        return {}

    results = dict()

    for model in model_directories:
        # Get split results
        model_base = os.path.join(experiment_base, model)
        model_splits = {}

        # Load all splits for this model
        for file in os.listdir(model_base):
            if (not file.endswith('.json')) or file == 'params.json':
                continue

            with open(os.path.join(model_base, file), 'r') as fp:
                model_splits[file] = json.load(fp)

        # Get all HRs and NDCGs for the different cutoffs
        hrs = defaultdict(list)
        ndcgs = defaultdict(list)

        for contents in model_splits.values():
            for k in range(1, upper_cutoff + 1):
                k = str(k)

                hrs[k].append(contents['hr'][k])
                ndcgs[k].append(contents['ndcg'][k])

        # Get mean and std for HR and NDCG at each cutoff
        hr = dict()
        ndcg = dict()
#         print(hr)
#         print(ndcg)
        for k in range(1, upper_cutoff + 1):
            k = str(k)

            if k not in hrs or k not in ndcgs:
                continue

            hr[k] = {
                'mean': np.mean(hrs[k]) if hrs[k] else np.nan,
                'std': np.std(hrs[k]) if hrs[k] else np.nan
            }

            ndcg[k] = {
                'mean': np.mean(ndcgs[k]) if ndcgs[k] else np.nan,
                'std': np.std(ndcgs[k]) if ndcgs[k] else np.nan
            }

        if hr and ndcg:
            results[model] = {'hr': hr, 'ndcg': ndcg}

    return results

def summarise(experiment_base):
    summary_path = os.path.join(experiment_base, 'summary.json')
    with open(summary_path, 'w') as fp:
        json.dump(get_summary(experiment_base), fp)
