import os
from typing import List
from process.data.Set import Set
from utility.Env import Env

class Data:
    def __init__(self, db_name):
        self.db_name = db_name
        path = os.path.join(Env().PATH + 'data', db_name)
        self.triples_path = os.path.join(path, 'triples.csv')

        self.sets_path = []
        set_path = path + '/set'
        for set in os.listdir(set_path):
            full_path = os.path.join(set_path, set)
            self.sets_path.append(full_path)

    def sets(self):
        for path in self.sets_path:
            yield Set(self, path)
