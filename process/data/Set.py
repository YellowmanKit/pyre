import os
from process.data.Split import Split

class Set:
    def __init__(self, parent, path):
        self.data = parent
        self.split_paths = []
        self.name = os.path.basename(path)

        for file in os.listdir(path):
            self.split_paths.append(os.path.join(path, file))
        self.split_paths = sorted(self.split_paths)

    def splits(self):
        for path in self.split_paths:
            yield Split(self, path)
