import json
import os

from utility.Class import Rating

class Split:
    def __init__(self, parent, path):
        self.set = parent
        self.name = os.path.basename(path)

        with open(path, 'r') as fp:
            data = json.load(fp)

            self.testing = data['testing']
            self.validation = data['validation']
            self.training = []

            helpers = set()
            descriptive_entities = set()
            users = set()

            for user, ratings in data['training']:
                user_ratings = list()
                users.add(user)

                for rating in ratings:
                    is_helper = rating['is_helper']
                    e_idx = rating['e_idx']

                    user_ratings.append(Rating(rating))
                    helpers.add(e_idx) if is_helper else descriptive_entities.add(e_idx)

                self.training.append((user, user_ratings))

            self.n_users = max(users) + 1 if users else 0
            self.n_descriptive_entities = max(descriptive_entities) + 1 if descriptive_entities else 0
            self.n_helpers = max(helpers) + 1 if helpers else 0
            self.n_entities = max(self.n_helpers, self.n_descriptive_entities)
