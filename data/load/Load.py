from utility.Base import Base
from data.load.Save import Save
from process.data.Data import Data
from process.Suggest import Suggest
from utility.Env import Env
from utility.Query import Query
import json

class Load:

    def data(self):
        res = Query.post('clear')
        with open(Env().PATH + 'data/pyre.json', 'r', encoding='utf8') as f:
            data = json.load(f)
            for db in data:
                print(db)
                save = Save(Base().data(db))
                save.triples(data[db]['triples'])
                save.ratings(data[db]['ratings'])
                Suggest().gen(Data(db))
