from data.load.model.Ratings import Ratings
from extend.Operation import Operation
from utility.Base import Base
from os.path import join
import pandas as pd
import numpy as np
import json

class Save(Operation):

    def __init__(self, base):
        self.base = base

    def ratings(self, value):
        self.convert(value)
        train, validation, test = [], [], []
        ratings = Ratings(value)
        for u, rs in ratings.map.items():
            liked = ratings.get_liked(u)
            if len(liked) < 2:
                train.append((u, [r.value for r in rs]))
                continue
            val_pos = self.sample(liked)
            test_pos = self.sample(liked)
            self.remove(val_pos, rs)
            self.remove(test_pos, rs)
            ratings.counts = ratings.get_counts()

            val_pos = val_pos.e_idx
            test_pos = test_pos.e_idx

            val_neg = self.sample_negative(set(ratings.get_helpers(1)), set(ratings.get_seen(u) + [val_pos]), 100)
            test_neg = self.sample_negative(set(ratings.get_helpers(1)), set(ratings.get_seen(u) + [val_pos]), 100)

            train.append((u, [r.value for r in rs]))
            validation.append((u, (val_pos, val_neg)))
            test.append((u, (test_pos, test_neg)))

        path = Base.create(join(self.base.set, '0'))
        with open(join(path, '0.json'), 'w') as file:
            json.dump({
                'training': train,
                'validation': validation,
                'testing': test
            }, file)

    def triples(self, triples):
        self.indexes = {}
        self.count = 0
        array = np.array(triples)
        for row in range(array.shape[0]):
            if row == 0:
                continue
            for col in range(array[row].shape[0]):
                cell = array[row][col]
                if not cell in self.indexes:
                    self.indexes[cell] = self.count
                    self.count+=1
                array[row][col] = self.indexes[cell]
        df = pd.DataFrame(data=array[1:,:], columns=array[0,:])
        df.to_csv(join(self.base.db, 'triples.csv'), index=False)
        self.save_mapping()

    def convert(self, ratings):
        for index in range(len(ratings)):
            for item in ratings[index]:
                value = ratings[index][item]
                if item == 'rating' or item == 'is_helper':
                    continue
                if str(value) not in self.indexes:
                    self.indexes[str(value)] = self.count
                    self.count+=1
                ratings[index][item] = self.indexes[str(value)]
        self.save_mapping()

    def save_mapping(self):
        mapping = {}
        for index in self.indexes:
            mapping[self.indexes[index]] = index
        with open(join(self.base.db, 'mapping.json'), 'w') as fp:
            json.dump(mapping, fp)
