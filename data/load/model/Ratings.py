from collections import defaultdict
from utility.Class import Rating

class Ratings:

    def __init__(self, ratings):
        self.value = ratings
        self.map = self.get_map()
        self.counts = self.get_counts()
        self.availables = self.get_availables()

    def get_seen(self, user):
        result = []
        for u, rs in self.map.items():
            if u != user:
                continue
            for r in rs:
                if r.is_helper:
                    result.append(r)
        return [r.e_idx for r in result]

    def get_liked(self, user):
        result = []
        for u, rs in self.map.items():
            if u != user:
                continue
            for r in rs:
                if r.rating == 1 and r.e_idx in self.availables:
                    result.append(r)
        return result

    def get_counts(self):
        counts = defaultdict(lambda:0)
        for u, rs in self.map.items():
            for h in [r.e_idx for r in rs if r.is_helper == True]:
                counts[h] += 1
        return counts

    def get_availables(self):
        return self.get_helpers(2)

    def get_helpers(self, min):
        return [h for h, count in self.counts.items() if count >= min]

    def get_map(self):
        map = defaultdict(list)
        for r in self.value:
            map[r['u_idx']].append(Rating(r))
        return map
